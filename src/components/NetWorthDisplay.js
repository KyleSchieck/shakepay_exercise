import React from 'react'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

// Notes for shakepay
// This is the main component, App.js just initializes it and defines some ui to change props on it
// I would have liked to complete the stretch goal of but ran out of time
// I made it extensible so that it would be easier to add this functionality
// I chose highcharts becuase I have some experience with it and it can create nice looking graphs quickly

/**
 * Display an account's net worth over a period of time
 * props:
 *    txData: [{ tx data }] - transaction data (format from this example: https://shakepay.github.io/programming-exercise/web/transaction_history.json)
 *    conversion: { BTC_CAD, ETH_CAD } - data used to convert BTC or ETH to CAD
 *    from: Date - optional chart start date
 *    to: Date - optional chart end date
 */
export default function NetWorthDisplay(props) {

  let btcToCad = props.conversion? props.conversion.BTC_CAD : 0;
  let ethToCad = props.conversion? props.conversion.ETH_CAD : 0;

  // Copied from: https://blog.abelotech.com/posts/number-currency-formatting-javascript/
  function currencyFormat(num) {
    return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }

  // Process prop txData
  let datas = [];
  let historic = {
    "BTC": 0,
    "CAD": 0,
    "ETH": 0
  };
  if (props.txData) {

    // Can I assume data is in order of date? Seems so
    //const sortedTxData = props.txData.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt));
    const sortedTxData = props.txData;

    for (let idx = sortedTxData.length - 1; idx >= 0; idx--) {
      let tx = sortedTxData[idx];
      let amount = parseFloat(tx.amount);
      let type=tx.type;

      if (type === 'conversion') {
        historic[tx.from.currency] -= tx.from.amount;
        historic[tx.to.currency] += tx.to.amount;
      } else if (type === 'peer' || type === "external account") {
        let directionMultiple = 0;
        switch (tx.direction) {
          case 'credit':
            directionMultiple = 1;
          break;
          case 'debit':
            directionMultiple = -1;
          break;
          default:
            console.error("Unexpected tx direction: " + tx.direction);
        }
        historic[tx.currency] += amount * directionMultiple;
      } else {
        console.error("Unexpected tx type: " + tx.type);
      }

      let historicResult = historic["CAD"] + (historic["BTC"] * btcToCad) + (historic["ETH"] * ethToCad);

      let date = new Date(tx.createdAt);
      datas.push({x: date, y: historicResult, tx: {...tx}, historic: {...historic}});
    }
  }

  // Create highchart options
  const options = {
    chart: {
        type: 'area'
    },
    title: {
        text: 'Net Worth'
    },
    tooltip: {
      enabled: true,
      formatter: function tooltipFormatter() {
        // Format the mouseover tooltip for a single point
        let historic = this.point.historic;
        let cadValue = "<span>CAD Value: $" + currencyFormat(this.point.y) + "</span><br/>";
        let date = "<span>" + new Date(this.point.x).toLocaleDateString() + "</span><br/><br/>";
        let btc = "<span>CAD: $" + currencyFormat(historic.CAD) + "</span><br/>";
        let cad = "<span>BTC: " + currencyFormat(historic.BTC) + "</span><br/>";
        let eth = "<span>ETH: " + currencyFormat(historic.ETH) + "</span><br/>";
        return cadValue + date + btc + cad + eth;
      }
    },
    xAxis: {
      type: 'datetime',
      dateTimeLabelFormats: {
          day: '%e. %b',
          month: '%b/%y',
          year: '%Y'
      },
      title: {
          text: "Date"
      },
      min: props.from,
      max: props.to
    },
    yAxis: {
        title: {
            text: 'CAD'
        },
        formatter: function yAxisFormatter(){
          return this.value;
        }
    },
    series: {
      data: datas
    },
    legend: {
        enabled: false // Disable the legend, we only have 1 series anyways.
    }
  };

  return <HighchartsReact
      highcharts={Highcharts}
      options={options}
    />
    
}
