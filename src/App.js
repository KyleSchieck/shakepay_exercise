import './App.css';

import React, { useState, useEffect } from 'react';

import NetWorthDisplay from './components/NetWorthDisplay.js'

function App() {

  const [txData, setTxData] = useState(null);
  const [conversionData, setConversionData] = useState(null);
  const [startDate, setStartDate] = useState(Date.parse('01 Jan 2018'));
  const [endDate, setEndDate] = useState(Date.parse('01 Jan 2020'));

  function getData(url, callback) {
    const Http = new XMLHttpRequest();
    Http.open("GET", url);
    Http.send();
    Http.onreadystatechange = (e) => {
      if (e.target.readyState === 4) {
        if (e.target.status === 200) {
          callback(JSON.parse(Http.responseText));
        } else {
          // TODO display error to user
          console.error("Failed to load data");
        }
      }
    }
  }

  // Copied from https://stackoverflow.com/questions/23593052/format-javascript-date-as-yyyy-mm-dd
  function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
  }
 

  // Load the data on initial mount
  useEffect(() => {
    getData('https://shakepay.github.io/programming-exercise/web/transaction_history.json', setTxData);

    // I tried to load this from my local app, but it is not allowing me due to origin access control.
    //getData('https://api.shakepay.co/rates');
    // Manually apply data
    setConversionData({"CAD_BTC":0.00001402,"BTC_CAD":71291.84,"CAD_ETH":0.000450184961458696,"ETH_CAD":2221.3,"USD_BTC":0.00001755,"BTC_USD":56976.27,"USD_ETH":0.000563296183950001,"ETH_USD":1775.26,"BTC_ETH":32.09242618741977,"ETH_BTC":0.03116,"CAD_USD":0.79,"USD_CAD":1.25});

  }, []); // [] as second argument to avoid running this after every render

  return (
    <div className="App">
      <NetWorthDisplay txData={txData} conversion={conversionData} from={startDate} to={endDate} />

      <h3>Interact with the component</h3>
      Start: <input type="date" value={formatDate(startDate)} onChange={e => setStartDate(Date.parse(e.target.value))} />&nbsp;
      End: <input type="date" value={formatDate(endDate)} onChange={e => setEndDate(Date.parse(e.target.value))} />
    </div>
  );
}

export default App;
